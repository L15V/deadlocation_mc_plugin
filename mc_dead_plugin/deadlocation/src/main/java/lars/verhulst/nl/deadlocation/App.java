package lars.verhulst.nl.deadlocation;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.Location;

import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import org.bukkit.plugin.java.JavaPlugin;
/**
 * Hello world!
 *
 */
public class App extends JavaPlugin implements Listener, CommandExecutor
{
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        getLogger().info("Deadlocation enabled!");
    }
    @Override
    public void onDisable() {
        getLogger().info("Deadlocation disabled!");
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player p = event.getEntity();
        Location l = p.getLocation();
        String x = String.format("%.0f", l.getX());
        String y = String.format("%.0f", l.getY());
        String z = String.format("%.0f", l.getZ());
        p.sendMessage("Your Death Coordinates: x: " + x + " y: " + y + " z: " + z);
    }

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
